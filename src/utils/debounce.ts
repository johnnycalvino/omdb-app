  
const debounce = (callback: any, interval: number = 300) =>{
  let timerId: number | undefined

  return (...args: any[]) => {
    if (timerId !== undefined) {
      clearTimeout(timerId)
    }

    timerId = setTimeout(() => {
      callback(args)
    }, interval)
  }
}

export default debounce