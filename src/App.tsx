import React from 'react'
import { Switch, Route } from 'react-router-dom'

import Dashboard from '@pages/dashboard'
import Favorites from '@pages/favorites'
import Detail from '@pages/detail'

import Header from '@components/header'

const App: React.VFC = () => (
  <>
    <Header />
    <Switch>
      <Route exact path="/favorites">
        <Favorites />
      </Route>
      <Route path="/detail/:id">
        <Detail />
      </Route>
      <Route exact path={['/', '/:page', '/:search']}>
        <Dashboard />
      </Route>
    </Switch>
  </>
)

export default App
