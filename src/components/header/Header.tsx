import React from 'react'
import { Link, useLocation } from 'react-router-dom'

import useLocalStorage from '@hooks/useLocalStorage'

import Logo from '@icons/logo'

const Header: React.VFC = () => {
  const location = useLocation()
  const [getLastResults] = useLocalStorage<{
    page: number
  }>('lastResults', [])
  const lastResults = getLastResults()
  const showBackButton = location.pathname.includes('detail')
  const hideFavoriteButton = location.pathname.includes('favorite')

  return (
    <header className="text-gray-600 body-font bg-gray-100">
      <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
        <Link to="/" className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0">
          <Logo />
          <span className="ml-3 text-xl">OMDB Movies</span>
        </Link>
        <nav className="md:ml-auto flex flex-wrap items-center text-base justify-center">
          {showBackButton && (
            <Link
              to={lastResults?.page ? `/${lastResults.page}` : '/'}
              className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center"
            >
              <svg
                className="h-5 w-5"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                  clipRule="evenodd"
                />
              </svg>
              <span>Back</span>
            </Link>
          )}
          {!hideFavoriteButton ? (
            <Link to="/favorites" className="ml-5 hover:text-gray-900">
              Favorites
            </Link>
          ) : (
            <Link to="/" className="ml-5 hover:text-gray-900">
              Home
            </Link>
          )}
        </nav>
      </div>
    </header>
  )
}

export default Header
