import React from 'react'
import { Link } from 'react-router-dom'

import Star from '@icons/star'
import Arrow from '@icons/arrow'

import styles from './card.module.css'
const Card: React.VFC<{
  title: string
  image: string
  type: string
  id: string
  isFav: boolean | undefined
  setFav: () => void
}> = ({ title, image, type, id, isFav, setFav }) => (
  <div className={styles.card}>
    <img
      className={styles.image}
      src={image !== 'N/A' ? image : 'https://dummyimage.com/350x450/ed8585/666&text=Not-found'}
      alt={title}
    />
    <div className={styles.menuContent}>
      <ul>
        <li>
          <p className={styles.type}>{type?.toUpperCase()}</p>
        </li>
        <li>
          <button className={styles.favBtn} onClick={setFav}>
            <Star isActive={isFav} />
          </button>
        </li>
      </ul>
    </div>
    <div className={styles.footer}>
      <h4 className={styles.title}>{title}</h4>
      <Link to={`detail/${id}`} className={styles.moreInfo}>
        Learn More
        <Arrow />
      </Link>
    </div>
  </div>
)

export default Card
