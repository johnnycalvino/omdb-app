import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

import { Pager, Paginator } from '@customTypes/paginator'

const Pagination: React.VFC<{
  actualPage: number
  totalItems: number
  size: number
}> = ({ actualPage, totalItems, size }) => {
  const [pager, setPager] = useState<Paginator>({
    totalItems: 1,
    currentPage: 1,
    pageSize: 1,
    totalPages: 1,
    startPage: 1,
    endPage: 1,
    startIndex: 1,
    endIndex: 1,
    pages: [1],
  })

  const getPager = ({ totalItems, page, size }: Pager) => {
    const currentPage = page || 1
    const pageSize = 10
    const totalPages = Math.ceil(totalItems / pageSize)

    let startPage = 0
    let endPage = 0
    if (totalPages <= 10) {
      startPage = 1
      endPage = totalPages
    } else {
      if (currentPage <= 6) {
        startPage = 1
        endPage = 10
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9
        endPage = totalPages
      } else {
        startPage = currentPage - 5
        endPage = currentPage + 4
      }
    }

    const startIndex = (currentPage - 1) * pageSize
    const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1)
    const pages = [...Array(endPage + 1 - startPage).keys()].map(i => startPage + i)

    setPager({
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages,
    })
  }

  useEffect(() => {
    getPager({ totalItems, page: actualPage, size })
  }, [actualPage, totalItems])

  return (
    <div className="bg-white px-4 py-3 flex items-center justify-between border-t border-b border-gray-200 sm:px-6">
      <div className="flex-1 flex justify-between sm:hidden">
        <Link
          to={actualPage - 1 === 0 ? '#' : `page=${actualPage - 1}`}
          className={`${
            actualPage - 1 === 0 && 'pointer-events-none'
          } relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-100`}
        >
          Anterior
        </Link>
        <Link
          to={actualPage + 1 === pager.pageSize ? '#' : `page=${actualPage + 1}`}
          className={`${
            actualPage + 1 === pager.pageSize && 'pointer-events-none'
          } ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-100`}
        >
          Siguiente
        </Link>
      </div>
      <div className="hidden sm:flex-1 sm:flex sm:items-center justify-center">
        <nav className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
          <Link
            to={actualPage - 1 === 0 ? '#' : `page=${actualPage - 1}`}
            className={`${
              actualPage - 1 === 0 && 'pointer-events-none'
            } relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-100`}
          >
            <span className="sr-only">Previous</span>
            <svg
              className="h-5 w-5"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </Link>
          {pager?.pages.map(pageNumber => {
            const active =
              'z-10 bg-indigo-50 border-indigo-500 text-indigo-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium'
            const inactive =
              'bg-white border-gray-300 text-gray-500 hover:bg-gray-100 hidden md:inline-flex relative items-center px-4 py-2 border text-sm font-medium'
            const styles = pageNumber === actualPage ? active : inactive
            return (
              <Link
                to={`page=${pageNumber}`}
                key={pageNumber}
                aria-current={pageNumber === actualPage ? 'page' : false}
                className={styles}
              >
                {pageNumber}
              </Link>
            )
          })}

          <Link
            to={actualPage + 1 === pager.pageSize ? '#' : `page=${actualPage + 1}`}
            className={`${
              actualPage + 1 === pager.pageSize && 'pointer-events-none'
            } relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-100`}
          >
            <span className="sr-only">Next</span>
            <svg
              className="h-5 w-5"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              aria-hidden="true"
            >
              <path
                fillRule="evenodd"
                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                clipRule="evenodd"
              />
            </svg>
          </Link>
        </nav>
      </div>
    </div>
  )
}

export default Pagination
