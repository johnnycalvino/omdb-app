import React from 'react'

import { Link } from 'react-router-dom'

import Logo from '@icons/logo'
import Www from '@icons/www'
import Twitter from '@icons/twitter'
import Linkedin from '@icons/linkedin'

const Footer = () => (
  <footer className="text-gray-600 body-font bg-gray-100">
    <div className="container px-5 py-8 mx-auto flex items-center sm:flex-row flex-col">
      <Link to="/" className="flex title-font font-medium items-center md:justify-start justify-center text-gray-900">
        <Logo />
        <span className="ml-3 text-xl">OMDB Movies</span>
      </Link>
      <span className="inline-flex sm:ml-auto sm:mt-0 mt-4 justify-center sm:justify-start">
        <a href="https://joca.dev" className="text-gray-500">
          <Www />
        </a>
        <a href="https://twitter.com/Gantit" className="ml-3 text-gray-500">
          <Twitter />
        </a>
        <a href="https://www.linkedin.com/in/jocadev/" className="ml-3 text-gray-500">
          <Linkedin />
        </a>
      </span>
    </div>
  </footer>
)

export default Footer
