import React from 'react'

const SearchBar: React.VFC<{
  searchValue: string
  onChangeValue: (value: string) => void
}> = ({ searchValue, onChangeValue }) => (
  <section className="text-gray-600 body-font  bg-gray-100">
    <div className="container px-5 pb-5 mx-auto">
      <div className="flex w-full justify-center items-end">
        <div className="relative mr-4 md:w-full lg:w-full xl:w-1/2 w-2/4">
          <label htmlFor="hero-field" className="leading-7 text-sm text-gray-600">
            Search for your favorite movie
          </label>
          <input
            type="text"
            id="hero-field"
            value={searchValue}
            onChange={e => onChangeValue(e.target.value)}
            name="hero-field"
            className="w-full rounded border bg-opacity-50 border-gray-300 focus:ring-2 focus:ring-red-200 focus:bg-transparent focus:border-red-500 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out"
          />
        </div>
      </div>
    </div>
  </section>
)

export default SearchBar
