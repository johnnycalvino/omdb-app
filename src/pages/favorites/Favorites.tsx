import React, { useState } from 'react'

import Card from '@components/card'

import useLocalStorage from '@hooks/useLocalStorage'

import Movie from '@customTypes/movie'

const Favorites = () => {
  const [getFavorites, setFavorites] = useLocalStorage<Movie[]>('favoritesList', [])
  const favoriteMovies = getFavorites()
  const [movies, setMovies] = useState(favoriteMovies)

  const toggleFavorite = ({ movie }: { movie: Movie }) => {
    const favorites = getFavorites() || []
    const newFavorites = [...favorites]
    const favoritesIdList = favorites.map(({ imdbID }) => imdbID)
    const index = favoritesIdList.indexOf(movie.imdbID as never)

    if (index > -1) {
      newFavorites.splice(index, 1)
    } else {
      newFavorites.push({ ...movie, isFav: true })
    }
    const newMovies = [...new Set(newFavorites)]
    setFavorites(newMovies)
    setMovies(newMovies)
  }

  return (
    <section className="text-gray-600 body-font">
      <div className="container px-5 py-24 mx-auto">
        {!movies && (
          <section className="text-gray-600 body-font">
            <div className="container mx-auto flex px-5 py-14 items-center justify-center flex-col">
              <img
                className="lg:w-2/6 md:w-3/6 w-5/6 mb-10 object-cover object-center rounded"
                alt="hero"
                src={'https://media.giphy.com/media/wJOXhSAlyJFUTV4mPf/giphy.gif'}
              />
              <div className="text-center lg:w-2/3 w-full">
                <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">no have favorites</h1>
              </div>
            </div>
          </section>
        )}
        <div className="flex flex-wrap -m-4 justify-center">
          {movies?.map((movie: Movie, index) => (
            <div key={`${movie.imdbID}-${index}`} className="p-4 lg:w-1/4 md:w-1/3 sm:w-1/2">
              <p>{movie.isFav ? 'true' : 'false'}</p>
              <Card
                title={movie.Title}
                image={movie.Poster}
                type={movie.Type}
                id={movie.imdbID}
                isFav={movie.isFav}
                setFav={() => toggleFavorite({ movie })}
              />
            </div>
          ))}
        </div>
      </div>
    </section>
  )
}
export default Favorites
