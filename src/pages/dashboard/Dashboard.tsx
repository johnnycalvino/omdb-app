import React, { useState, useEffect, useRef, MutableRefObject } from 'react'
import { useParams, useHistory } from 'react-router-dom'
// @ts-ignore:noImplicitAny
import Bounce from 'react-reveal/Bounce'

import Card from '@components/card'
import Pagination from '@components/pagination'
import SearchBar from '@components/searchBar'
import Footer from '@components/footer'

import debounce from '@utils/debounce'
import useLocalStorage from '@hooks/useLocalStorage'

import { Page } from '@customTypes/paginator'
import Movie from '@customTypes/movie'

const Dashboard: React.VFC = () => {
  const history = useHistory()
  const [getFavorites, setFavorites] = useLocalStorage<Movie[]>('favoritesList', [])
  const [getLastResults, setLastResults] = useLocalStorage<{
    searchValue: string
    search: Movie[]
    page: number
    totalResults: number
    pages: number
    actualPage: number
  }>('lastResults', [])
  const controllerRef = useRef<AbortController | null>()
  const { page, search } = useParams<{
    page?: string
    search?: string
  }>()
  const lastResults = getLastResults()

  const [movies, setMovies] = useState<Movie[]>([])
  const [searchValue, setSearchValue] = useState<string>(search || lastResults?.searchValue || '')
  const [error, setError] = useState<string>('')
  const [pagination, setPagination] = useState<Page>({
    totalResults: 1,
    pages: 1,
    actualPage: 1,
  })

  const updateFavoriteMovieList = (movies: Movie[]) => {
    const localFavorites = getFavorites() || []
    const favoritesIdList = localFavorites.map(({ imdbID }) => imdbID)
    const newMovies = movies?.map((movie: Movie) => ({
      ...movie,
      isFav: favoritesIdList.includes(movie.imdbID),
    }))
    setMovies(newMovies)
  }

  const getData = async (controllerRef: MutableRefObject<AbortController | null | undefined>): Promise<void> => {
    if (controllerRef.current) {
      controllerRef.current.abort()
    }
    if (!searchValue) {
      return
    }
    const controller = new AbortController()
    controllerRef.current = controller

    const currentPage = parseInt(page?.split('=')[1] || '1')
    let url = `https://www.omdbapi.com/?apikey=7d550524&s=${searchValue}`
    url += currentPage ? `&page=${currentPage}` : ''
    const res = await fetch(url, {
      signal: controllerRef.current?.signal,
    })
    const jsonData = await res.json()
    const { Error: error, Search: search, totalResults } = jsonData
    setError('')
    if (error) {
      setError(error)
    }

    updateFavoriteMovieList(search)
    setLastResults({
      searchValue,
      search,
      page,
      totalResults: parseInt(totalResults),
      pages: Math.ceil(totalResults / search?.length || 1),
      actualPage: currentPage,
    })

    setPagination({
      totalResults: parseInt(totalResults),
      pages: Math.ceil(totalResults / search?.length || 1),
      actualPage: currentPage,
    })

    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })

    if (controllerRef.current) {
      controllerRef.current = null
    }
  }

  useEffect(() => {
    const lastResults = getLastResults()
    if (!lastResults?.search) {
      getData(controllerRef)
    } else {
      updateFavoriteMovieList(lastResults?.search)
      setPagination({
        totalResults: lastResults?.totalResults,
        pages: lastResults?.pages,
        actualPage: lastResults?.actualPage,
      })
    }
    console.log(1)
  }, [])

  useEffect(() => {
    const lastResults = getLastResults()
    if (lastResults?.page !== page) {
      getData(controllerRef)
    }
  }, [page])

  useEffect(() => {
    const lastResults = getLastResults()
    if (searchValue.length > 2 && lastResults?.searchValue !== searchValue) {
      debounce(getData(controllerRef), 1000)
      if (lastResults?.page) {
        history.replace('/')
      }
    }
  }, [searchValue])

  const toggleFavorite = ({ movie }: { movie: Movie }) => {
    const favorites = getFavorites() || []
    const newFavorites = [...favorites]
    const favoritesIdList = favorites.map(({ imdbID }) => imdbID)

    const index = favoritesIdList.indexOf(movie.imdbID as never)

    if (index > -1) {
      newFavorites.splice(index, 1)
    } else {
      if (favoritesIdList.length >= 20) {
        alert('You can only have 20 favorite movies, if you want to add more feel free to hire me me@joca.dev')
        return
      }
      newFavorites.push({ ...movie, isFav: true })
    }

    setFavorites([...new Set(newFavorites)])
    updateFavoriteMovieList(movies)
  }

  return (
    <>
      <SearchBar searchValue={searchValue} onChangeValue={setSearchValue} />
      {movies && movies.length && (
        <Pagination actualPage={pagination.actualPage} totalItems={pagination.totalResults} size={pagination.pages} />
      )}
      <section className="text-gray-600 body-font">
        <div className="container px-5 py-24 mx-auto">
          {!(movies && movies.length) && (
            <section className="text-gray-600 body-font">
              <div className="container mx-auto flex px-5 py-14 items-center justify-center flex-col">
                <img
                  className="lg:w-2/6 md:w-3/6 w-5/6 mb-10 object-cover object-center rounded"
                  alt="hero"
                  src={
                    error
                      ? 'https://media.giphy.com/media/H7wajFPnZGdRWaQeu0/giphy.gif'
                      : 'https://media.giphy.com/media/wJOXhSAlyJFUTV4mPf/giphy.gif'
                  }
                />
                <div className="text-center lg:w-2/3 w-full">
                  <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">
                    {error || "let's look for a movie"}
                  </h1>
                </div>
              </div>
            </section>
          )}
          <Bounce left cascade>
            <div className="flex flex-wrap -m-4 justify-center">
              {movies?.map((movie: Movie, index) => (
                <div key={`${movie.imdbID}-${index}`} className="p-4 lg:w-1/4 md:w-1/3 sm:w-1/2">
                  <Card
                    title={movie.Title}
                    image={movie.Poster}
                    type={movie.Type}
                    id={movie.imdbID}
                    isFav={movie.isFav}
                    setFav={() => toggleFavorite({ movie })}
                  />
                </div>
              ))}
            </div>
          </Bounce>
        </div>
      </section>
      {movies && movies.length && (
        <Pagination actualPage={pagination.actualPage} totalItems={pagination.totalResults} size={pagination.pages} />
      )}
      <Footer />
    </>
  )
}

export default Dashboard
