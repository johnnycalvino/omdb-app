import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

import Movie from '@customTypes/movie'

import Star from '@icons/star'

const Detail: React.VFC = () => {
  const { id } = useParams<{ id?: string }>()
  const [detail, setDetails] = useState<Movie>()

  const getData = async () => {
    const res = await fetch(`https://www.omdbapi.com/?apikey=ec20d40e&i=${id}`)
    const data = await res.json()
    setDetails(data)
  }

  useEffect(() => {
    getData()
  }, [])

  const getRatingStart = (rating: string | undefined): any => {
    if (!rating) return null
    const numberRating: number = parseFloat(rating)
    const startsCount: number = Math.round((numberRating * 5) / 10)

    return [...Array(5).keys()].map((x, index) => <Star key={index} isActive={index < (startsCount || 0)} />)
  }

  return (
    <section className="text-gray-600 body-font overflow-hidden">
      <div className="container px-5 py-16 mx-auto">
        <div className="lg:w-4/5 mx-auto flex flex-wrap">
          <img
            alt="ecommerce"
            className="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded"
            src={
              detail && detail.Poster !== 'N/A'
                ? detail.Poster
                : 'https://dummyimage.com/300x444/ed8585/666&text=Not-found'
            }
          />
          <div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
            <h2 className="text-sm title-font text-gray-500 tracking-widest">{detail?.Type?.toUpperCase()}</h2>
            <h1 className="text-gray-900 text-3xl title-font font-medium mb-1">{detail?.Title}</h1>
            <div className="flex mb-4">
              <span className="flex items-center">
                {getRatingStart(detail?.imdbRating)}
                <span className="text-gray-600 ml-3">{detail?.imdbVotes} Votes</span>
              </span>
              <span className="flex ml-3 pl-3 py-2 border-l-2 border-gray-200 space-x-2s">{detail?.Year}</span>
            </div>
            <p className="leading-relaxed">{detail?.Plot}</p>
          </div>
        </div>
      </div>
    </section>
  )
}
export default Detail
