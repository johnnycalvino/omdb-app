
import Movie from '@customTypes/movie'

interface Paginator {
  totalItems: number
  currentPage: number
  pageSize: number
  totalPages: number
  startPage: number
  endPage: number
  startIndex: number
  endIndex: number
  pages: number[],
}

interface Pager {
  totalItems: number
  page: number
  size?: number
}

interface Page {
  totalResults:number
  pages:number
  actualPage: number
  results?: Movie[]
  error?: any
}

export type { Pager, Paginator, Page }