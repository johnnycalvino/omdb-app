import { useCallback, useEffect } from 'react'

type UseLocalStorage<V> = [() => V | null, (value: any) => void]
const useLocalStorage = <V,>(key: any, initialValue: any): UseLocalStorage<V> => {
  const keyfier = (key: any) => `@omdb-app:${key}`
  const valuefier = (value: any[]) => JSON.stringify(value)
  useEffect(() => {
    if (!localStorage.getItem(keyfier(key))) {
      localStorage.setItem(keyfier(key), valuefier(initialValue))
    }
  }, [key, initialValue])

  const getValue = useCallback(() => {
    try {
      const storageValue = localStorage.getItem(keyfier(key))
      if (storageValue) {
        return JSON.parse(storageValue) as V
      }
    } catch (error) {
      localStorage.removeItem(keyfier(key))
      console.error(error)
    }
    return null
  }, [key])

  const setValue = useCallback(value => localStorage.setItem(keyfier(key), valuefier(value)), [keyfier(key)])
  return [getValue, setValue]
}

export default useLocalStorage
