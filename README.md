# Omdb App ES
Para la creación de la app use [Vite](https://github.com/vitejs/vite), era una herramienta que quería probar ya que es muy rápida y aproveche la ocasión.

Dado a que lo que se busca evaluar es mi conocimiento he tomado unas decisiones que me han permitido ahorrarse tiempo y aun asi no he llegado hasta donde quería


### ARQUITECTURA
- La estructura de carpetas usadas en la con la que me he sentido más cómodo a lo largo de mi exp laboral
 - El uso de la librería de CCS [tailwindcss](https://tailwindcss.com) para agilizar en la creación de los componentes (cosa que más adelante me dio problemas)
- Aunque tengo tiempo sin usarlo he decidido poner [TypeScript](https://github.com/microsoft/TypeScript) para hacer un refresco sus “virtudes” y me a recordado lo mal que lo pasaba intentado tipar correctamente las variables, métodos, etc… Siendo sincero la verdad no lo echaba de menos. Me quedo a la espera de que me demuestren las virtudes que tiene en un proyecto de react.
- Está añadido también linter con standar [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/).
- En el tema de configuración del entorno, he usado los paths, para que los import queden más limpios. 

### ESTILOS
- Al usar tailwindcss me ha permitido sacar unas plantillas de base para ir aún más rápido en la creación de los mismos [tailblocks](https://tailblocks.cc/) 
- Para mostrar algo de mis conocimientos en css he maquetado el componente [CARD](src/components/card) ha sido con css puro ya que no considere necesario usar algún procesador para solo 1 componente


### DATOS EXTRA
- Tiempo invertido 14h
- No he podido agregar los test ya que vite no está preparado todavía para la librería de Jest con react-testin-library me dio muchos dolores intentar agregarla y no llegue nunca a buen puerto. (nota mental no usar vite si quiero poner test)
- Pensé en poder agregar algo de e2e pero tan tarde ya el sueño me ganaba

---
# Omdb App EN
For the creation of the app I used [Vite](https://github.com/vitejs/vite), it was a tool that I wanted to try because it is very fast and I took the opportunity.

Since what I wanted to evaluate is my knowledge, I made some decisions that allowed me to save time and even so I didn't get to where I wanted to be.


ARCHITECTURE
- The folder structure used in the one I have felt more comfortable with throughout my work experience.
 - The use of the CCS library [tailwindcss](https://tailwindcss.com) to speed up the creation of components (which later gave me problems).
- Although I have some time without using it I decided to put [TypeScript](https://github.com/microsoft/TypeScript) to refresh its "virtues" and it reminded me how bad I had a hard time trying to correctly type variables, methods, etc... To be honest I didn't miss it. I am waiting for them to demonstrate me the virtues that it has in a react project.
- It is also added linter with standard [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/).
- In the configuration of the environment, I have used the paths, so that the imports are cleaner. 

STYLES
- Using tailwindcss has allowed me to get some basic templates to go even faster in the creation of the same [tailblocks](https://tailblocks.cc/) 
- To show some of my knowledge in css I have designed the component [CARD](src/components/card) has been with pure css since I did not consider necessary to use some processor for only 1 component.


EXTRA DATA
- Time invested 14h
- I have not been able to add the tests because vite is not ready yet for the Jest library with react-testin-library it gave me a lot of pain to try to add it and I never got to the end. (mental note don't use vite if I want to put test)
- I thought about adding something from e2e but so late I was already sleepy.
